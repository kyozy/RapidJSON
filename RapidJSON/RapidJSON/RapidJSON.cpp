﻿// RapidJSON.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "RapidJSON.h"



extern RAPIDJSON_API rjDoc* rapidjson_docment_create()
{
	return new rjDoc();
}

extern RAPIDJSON_API void rapidjson_docment_destory(rjDoc* pDoc)
{
	if (pDoc)
	{
		delete pDoc;
	}
}

extern RAPIDJSON_API BOOL rapidjson_docment_parse(rjDoc* pDoc, const char* json)
{
	pDoc->Parse<rapidjson::kParseDefaultFlags>(json);
	return !pDoc->HasParseError();
}

extern RAPIDJSON_API BOOL rapidjson_docment_parse_insitu(rjDoc* pDoc, char* json)
{
	pDoc->ParseInsitu<rapidjson::kParseDefaultFlags>(json);
	return !pDoc->HasParseError();
}

extern RAPIDJSON_API rjErrorCode rapidjson_docment_get_parse_error(rjDoc* pDoc)
{
	return pDoc->GetParseError();
}

extern RAPIDJSON_API size_t rapidjson_docment_get_error_offset(rjDoc* pDoc)
{
	return pDoc->GetErrorOffset();
}

extern RAPIDJSON_API rjDoc* rapidjson_docment_swap(rjDoc* pDoc, rjDoc* rhs)
{
	return &pDoc->Swap(*rhs);
}

extern RAPIDJSON_API rjAllocator* rapidjson_docment_get_allocator(rjDoc* pDoc)
{
	return &pDoc->GetAllocator();
}

extern RAPIDJSON_API BOOL rapidjson_value_accept(rjValue* pVal, rjWriterSB* handler)
{
	return pVal->Accept(*handler);
}

extern RAPIDJSON_API rjType rapidjson_value_get_type(rjValue* pVal)
{
	return pVal->GetType();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_null(rjValue* pVal)
{
	return pVal->IsNull();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_false(rjValue * pVal)
{
	return pVal->IsFalse();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_true(rjValue * pVal)
{
	return pVal->IsTrue();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_bool(rjValue* pVal)
{
	return pVal->IsBool();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_object(rjValue* pVal)
{
	return pVal->IsObject();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_array(rjValue* pVal)
{
	return pVal->IsArray();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_number(rjValue* pVal)
{
	return pVal->IsNumber();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_int(rjValue* pVal)
{
	return pVal->IsInt();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_uint(rjValue* pVal)
{
	return pVal->IsUint();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_int64(rjValue* pVal)
{
	return pVal->IsInt64();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_uint64(rjValue* pVal)
{
	return pVal->IsUint64();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_double(rjValue* pVal)
{
	return pVal->IsDouble();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_string(rjValue* pVal)
{
	return pVal->IsString();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_lossless_double(rjValue* pVal)
{
	return pVal->IsLosslessDouble();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_float(rjValue* pVal)
{
	return pVal->IsFloat();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_lossless_float(rjValue* pVal)
{
	return pVal->IsLosslessFloat();
}

extern RAPIDJSON_API rjValue* rapidjson_value_create(rjType type)
{
	return new rjValue(type);
}

extern RAPIDJSON_API void rapidjson_value_destory(rjValue* pVal)
{
	if (pVal)
	{
		delete pVal;
	}
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_null(rjValue* pVal)
{
	return &pVal->SetNull();
}

extern RAPIDJSON_API BOOL rapidjson_value_get_bool(rjValue* pVal)
{
	return pVal->GetBool();
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_bool(rjValue* pVal, BOOL b)
{
	return &pVal->SetBool(b);
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_object(rjValue* pVal)
{
	return &pVal->SetObject();
}

extern RAPIDJSON_API rjSizeType rapidjson_value_get_member_count(rjValue* pVal)
{
	return pVal->MemberCount();
}


extern RAPIDJSON_API BOOL rapidjson_value_is_object_empty(rjValue* pVal)
{
	return pVal->ObjectEmpty();
}


extern RAPIDJSON_API int rapidjson_value_get_members(rjValue* pVal, const char** names, rjValue** values)
{
	if (!pVal->IsObject())
	{
		return 0;
	}
	int i = 0;
	for (rjValue::MemberIterator iter = pVal->MemberBegin(); iter != pVal->MemberEnd(); iter++)
	{
		if (names)
		{
			names[i] = iter->name.GetString();
		}
		if (values)
		{
			values[i] = &iter->value;
		}
		++i;
	}
	return i;
}

extern RAPIDJSON_API BOOL rapidjson_value_has_member(rjValue* pVal, const char* name)
{
	return pVal->HasMember(name);
}

extern RAPIDJSON_API rjValue* rapidjson_value_find_member(rjValue* pVal, const char* name)
{
	rjValue::MemberIterator val = pVal->FindMember(name);
	if (val == pVal->MemberEnd())
	{
		return NULL;
	}
	return &val->value;
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member(rjValue* pVal, const char* name, rjValue* val, rjAllocator* allocator)
{
	return &pVal->AddMember(rjValue(name, *allocator), *val, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_null(rjValue* pVal, const char* name, rjAllocator* allocator)
{
	rjValue val(rapidjson::kNullType);
	return &pVal->AddMember(rjValue(name, *allocator), val, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_bool(rjValue* pVal, const char* name, BOOL val, rjAllocator* allocator)
{
	return &pVal->AddMember(rjValue(name, *allocator), rjValue((bool)val), *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_int(rjValue* pVal, const char* name, int val, rjAllocator* allocator)
{
	rjValue rjval(val);
	return &pVal->AddMember(rjValue(name, *allocator), rjval, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_int64(rjValue* pVal, const char* name, __int64 val, rjAllocator* allocator)
{
	rjValue rjval(val);
	return &pVal->AddMember(rjValue(name, *allocator), rjval, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_float(rjValue* pVal, const char* name, float val, rjAllocator* allocator)
{
	rjValue rjval(val);
	return &pVal->AddMember(rjValue(name, *allocator), rjval, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_double(rjValue* pVal, const char* name, double val, rjAllocator* allocator)
{
	rjValue rjval(val);
	return &pVal->AddMember(rjValue(name, *allocator), rjval, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_string(rjValue* pVal, const char* name, const char* val, rjSizeType length, rjAllocator* allocator)
{
	rjValue rjval(val, length, *allocator);
	return &pVal->AddMember(rjValue(name, *allocator), rjval, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_array(rjValue* pVal, const char* name, rjAllocator* allocator)
{
	rjValue val(rapidjson::kArrayType);
	return &pVal->AddMember(rjValue(name, *allocator), val, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_add_member_object(rjValue* pVal, const char* name, rjAllocator* allocator)
{
	rjValue val(rapidjson::kObjectType);
	return &pVal->AddMember(rjValue(name, *allocator), val, *allocator);
}


extern RAPIDJSON_API void rapidjson_value_remove_all_members(rjValue* pVal)
{
	pVal->RemoveAllMembers();
}

extern RAPIDJSON_API BOOL rapidjson_value_remove_member(rjValue* pVal, const char* name)
{
	return pVal->RemoveMember(name);
}

extern RAPIDJSON_API BOOL rapidjson_value_erase_member(rjValue* pVal, const char* name)
{
	return pVal->EraseMember(name);
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_array(rjValue* pVal)
{
	return &pVal->SetArray();
}

extern RAPIDJSON_API rjSizeType rapidjson_value_get_size(rjValue* pVal)
{
	return pVal->Size();
}

extern RAPIDJSON_API rjSizeType rapidjson_value_get_capacity(rjValue* pVal)
{
	return pVal->Capacity();
}

extern RAPIDJSON_API BOOL rapidjson_value_is_empty(rjValue* pVal)
{
	return pVal->Empty();
}

extern RAPIDJSON_API void rapidjson_value_clear(rjValue* pVal)
{
	pVal->Clear();
}

extern RAPIDJSON_API rjValue* rapidjson_value_get_at(rjValue* pVal, int index)
{
	return &(*pVal)[index];
}

extern RAPIDJSON_API int rapidjson_value_get_values(rjValue* pVal, rjValue** values)
{
	int i = 0;
	for (rjValue::ValueIterator iter = pVal->Begin(); iter != pVal->End(); iter++)
	{
		if (values)
		{
			values[i] = iter;
		}
		++i;
	}
	return i;
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback(rjValue* pVal, rjValue* val, rjAllocator* allocator)
{
	return &pVal->PushBack(*val, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_null(rjValue* pVal, rjAllocator* allocator)
{
	rjValue rjVal(rapidjson::kNullType);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_bool(rjValue* pVal, BOOL val, rjAllocator* allocator)
{
	rjValue rjVal((bool)val);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_int(rjValue* pVal, int val, rjAllocator* allocator)
{
	rjValue rjVal(val);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_int64(rjValue* pVal, __int64 val, rjAllocator* allocator)
{
	rjValue rjVal(val);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_float(rjValue* pVal, float val, rjAllocator* allocator)
{
	rjValue rjVal(val);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_double(rjValue* pVal, double val, rjAllocator* allocator)
{
	rjValue rjVal(val);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_string(rjValue* pVal, const char* val, rjSizeType length, rjAllocator* allocator)
{
	rjValue rjVal(val, length, *allocator);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_array(rjValue* pVal, rjAllocator* allocator)
{
	rjValue rjVal(rapidjson::kArrayType);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_pushback_object(rjValue* pVal, rjAllocator* allocator)
{
	rjValue rjVal(rapidjson::kObjectType);
	return &pVal->PushBack(rjVal, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_popback(rjValue* pVal)
{
	return &pVal->PopBack();
}

extern RAPIDJSON_API rjValue* rapidjson_value_erase(rjValue* pVal, int index, int count)
{
	rjValue::ValueIterator iter = pVal->Begin();
	iter += index;
	if (count == 0)
	{
		return pVal->Erase(iter);
	}
	else
	{
		return pVal->Erase(iter, iter + count);
	}
}

extern RAPIDJSON_API int rapidjson_value_get_int(rjValue* pVal)
{
	return pVal->GetInt();
}

extern RAPIDJSON_API int64_t rapidjson_value_get_int64(rjValue* pVal)
{
	return pVal->GetInt64();
}

extern RAPIDJSON_API double rapidjson_value_get_double(rjValue* pVal)
{
	return pVal->GetDouble();
}

extern RAPIDJSON_API float rapidjson_value_get_float(rjValue* pVal)
{
	return pVal->GetFloat();
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_int(rjValue* pVal, int i)
{
	return &pVal->SetInt(i);
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_int64(rjValue* pVal, int64_t i64)
{
	return &pVal->SetInt64(i64);
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_double(rjValue* pVal, double d)
{
	return &pVal->SetDouble(d);
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_float(rjValue* pVal, float f)
{
	return &pVal->SetFloat(f);
}

extern RAPIDJSON_API const char* rapidjson_value_get_string(rjValue* pVal)
{
	return pVal->GetString();
}

extern RAPIDJSON_API rjSizeType rapidjson_value_get_string_length(rjValue* pVal)
{
	return pVal->GetStringLength();
}

extern RAPIDJSON_API rjValue* rapidjson_value_set_string(rjValue* pVal, const char* str, rjSizeType length, rjAllocator* allocator)
{
	return &pVal->SetString(str, length, *allocator);
}

extern RAPIDJSON_API rjValue* rapidjson_value_swap(rjValue* pVal, rjValue* r)
{
	return &pVal->Swap(*r);
}

extern RAPIDJSON_API rjValue* rapidjson_value_copy(rjValue* pVal, rjValue* r, rjAllocator* allocator)
{
	return &pVal->CopyFrom(*r, *allocator);
}

extern RAPIDJSON_API rjStringBuffer* rapidjson_string_buffer_create(size_t capacity)
{
	if (capacity <= 0)
	{
		capacity = rjStringBuffer::kDefaultCapacity;
	}
	return new rjStringBuffer(0, capacity);
}

extern RAPIDJSON_API void rapidjson_string_buffer_destory(rjStringBuffer* pStrbuf)
{
	if (pStrbuf)
	{
		delete pStrbuf;
	}
}

extern RAPIDJSON_API void rapidjson_string_buffer_put(rjStringBuffer* pStrbuf, char c)
{
	pStrbuf->Put(c);
}

extern RAPIDJSON_API void rapidjson_string_buffer_putunsafe(rjStringBuffer* pStrbuf, char c)
{
	pStrbuf->PutUnsafe(c);
}

extern RAPIDJSON_API void rapidjson_string_buffer_flush(rjStringBuffer* pStrbuf)
{
	pStrbuf->Flush();
}

extern RAPIDJSON_API void rapidjson_string_buffer_clear(rjStringBuffer* pStrbuf)
{
	pStrbuf->Clear();
}

extern RAPIDJSON_API void rapidjson_string_buffer_shrink_to_fit(rjStringBuffer* pStrbuf)
{
	pStrbuf->ShrinkToFit();
}

extern RAPIDJSON_API void rapidjson_string_buffer_reserve(rjStringBuffer* pStrbuf, size_t count)
{
	pStrbuf->Reserve(count);
}

extern RAPIDJSON_API char* rapidjson_string_buffer_push(rjStringBuffer* pStrbuf, size_t count)
{
	return pStrbuf->Push(count);
}

extern RAPIDJSON_API char* rapidjson_string_buffer_pushunsafe(rjStringBuffer* pStrbuf, size_t count)
{
	return pStrbuf->PushUnsafe(count);
}

extern RAPIDJSON_API void rapidjson_string_buffer_pop(rjStringBuffer* pStrbuf, size_t count)
{
	pStrbuf->Pop(count);
}

extern RAPIDJSON_API const char* rapidjson_string_buffer_get_string(rjStringBuffer* pStrbuf)
{
	return pStrbuf->GetString();
}

extern RAPIDJSON_API size_t rapidjson_string_buffer_get_size(rjStringBuffer* pStrbuf)
{
	return pStrbuf->GetSize();
}


extern RAPIDJSON_API rjWriterSB* rapidjson_writer_create(rjStringBuffer* pStrbuf, size_t levelDepth)
{
	if (levelDepth <=0)
	{
		levelDepth = rjWriterSB::kDefaultMaxDecimalPlaces;
	}
	return new rjWriterSB(*pStrbuf,0,levelDepth);
}

extern RAPIDJSON_API void rapidjson_writer_destory(rjWriterSB* pWriter)
{
	if (pWriter)
	{
		delete pWriter;
	}
}

extern RAPIDJSON_API void rapidjson_writer_reset(rjWriterSB* pWriter, rjStringBuffer* pStrbuf)
{
	pWriter->Reset(*pStrbuf);
}

extern RAPIDJSON_API BOOL rapidjson_writer_is_complete(rjWriterSB* pWriter)
{
	return pWriter->IsComplete();
}

extern RAPIDJSON_API int rapidjson_writer_get_max_decimal_places(rjWriterSB* pWriter)
{
	return pWriter->GetMaxDecimalPlaces();
}

extern RAPIDJSON_API void rapidjson_writer_set_max_decimal_places(rjWriterSB* pWriter, int maxDecimalPlaces)
{
	pWriter->SetMaxDecimalPlaces(maxDecimalPlaces);
}

extern RAPIDJSON_API BOOL rapidjson_writer_rawvalue(rjWriterSB* pWriter, const char *json, size_t length, rjType type)
{
	return pWriter->RawValue(json, length, type);
}


extern RAPIDJSON_API BOOL rapidjson_writer_null(rjWriterSB* pWriter)
{
	return pWriter->Null();
}

extern RAPIDJSON_API BOOL rapidjson_writer_bool(rjWriterSB* pWriter, BOOL b)
{
	return pWriter->Bool(b);
}

extern RAPIDJSON_API BOOL rapidjson_writer_int(rjWriterSB* pWriter, int i)
{
	return pWriter->Int(i);
}

extern RAPIDJSON_API BOOL rapidjson_writer_uint(rjWriterSB* pWriter, unsigned u)
{
	return pWriter->Uint(u);
}

extern RAPIDJSON_API BOOL rapidjson_writer_int64(rjWriterSB* pWriter, __int64 i64)
{
	return pWriter->Int64(i64);
}

extern RAPIDJSON_API BOOL rapidjson_writer_uint64(rjWriterSB* pWriter, unsigned __int64 u64)
{
	return pWriter->Uint64(u64);
}

extern RAPIDJSON_API BOOL rapidjson_writer_double(rjWriterSB* pWriter, double d)
{
	return pWriter->Double(d);
}

extern RAPIDJSON_API BOOL rapidjson_writer_rawnumber(rjWriterSB* pWriter, const char *str, rjSizeType length, BOOL copy)
{
	return pWriter->RawNumber(str, length, (bool)copy);
}

extern RAPIDJSON_API BOOL rapidjson_writer_string(rjWriterSB* pWriter, const char *str, rjSizeType length, BOOL copy)
{
	return pWriter->String(str, length, (bool)copy);
}

extern RAPIDJSON_API BOOL rapidjson_writer_startobject(rjWriterSB* pWriter)
{
	return pWriter->StartObject();
}

extern RAPIDJSON_API BOOL rapidjson_writer_key(rjWriterSB* pWriter, const char *str, rjSizeType length, BOOL copy)
{
	return pWriter->Key(str, length, (bool)copy);
}

extern RAPIDJSON_API BOOL rapidjson_writer_endobject(rjWriterSB* pWriter, rjSizeType memberCount)
{
	return pWriter->EndObject(memberCount);
}

extern RAPIDJSON_API BOOL rapidjson_writer_startarray(rjWriterSB* pWriter)
{
	return pWriter->StartArray();
}

extern RAPIDJSON_API BOOL rapidjson_writer_endarray(rjWriterSB* pWriter, rjSizeType memberCount)
{
	return pWriter->EndArray(memberCount);
}

extern RAPIDJSON_API rjPointer* rapidjson_pointer_create(const char* source)
{
	return new rjPointer(source);
}

extern RAPIDJSON_API void rapidjson_pointer_destory(rjPointer* pPointer)
{
	if (pPointer)
	{
		delete pPointer;
	}
}

extern RAPIDJSON_API BOOL rapidjson_pointer_erase(rjPointer* pPointer, rjValue* root)
{
	return pPointer->Erase(*root);
}

extern RAPIDJSON_API rjPointer* rapidjson_pointer_append(rjPointer* pPointer, const char* name)
{
	return new rjPointer(pPointer->Append(name));
}

extern RAPIDJSON_API BOOL rapidjson_pointer_isvalid(rjPointer* pPointer)
{
	return pPointer->IsValid();
}

extern RAPIDJSON_API size_t rapidjson_pointer_get_parse_error_offset(rjPointer* pPointer)
{
	return pPointer->GetParseErrorOffset();
}

extern RAPIDJSON_API rjPointerErrorCode rapidjson_pointer_get_parse_error_code(rjPointer* pPointer)
{
	return pPointer->GetParseErrorCode();
}

extern RAPIDJSON_API const rjToken* rapidjson_pointer_get_tokens(rjPointer* pPointer)
{
	return pPointer->GetTokens();
}

extern RAPIDJSON_API size_t rapidjson_pointer_get_token_count(rjPointer* pPointer)
{
	return pPointer->GetTokenCount();
}

extern RAPIDJSON_API BOOL rapidjson_pointer_stringify(rjPointer* pPointer, rjStringBuffer* pStrbuf)
{
	return pPointer->Stringify(*pStrbuf);
}

extern RAPIDJSON_API BOOL rapidjson_pointer_stringify_uri_fragment(rjPointer* pPointer, rjStringBuffer* pStrbuf)
{
	return pPointer->StringifyUriFragment(*pStrbuf);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_create_value2(rjPointer* pPointer, rjValue* root, rjAllocator* allocator, bool *alreadyExist /*= 0*/)
{
	return &pPointer->Create(*root, *allocator, alreadyExist);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_create_value(rjPointer* pPointer, rjDoc* pDoc, bool *alreadyExist /*= 0*/)
{
	return &pPointer->Create(*pDoc, alreadyExist);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get(rjPointer* pPointer, rjValue* root, size_t *unresolvedTokenIndex /*= 0*/)
{
	return pPointer->Get(*root, unresolvedTokenIndex);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default(rjPointer* pPointer, rjDoc* pDoc, rjValue* defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, *defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_int(rjPointer* pPointer, rjDoc* pDoc, int defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_uint(rjPointer* pPointer, rjDoc* pDoc, unsigned defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_int64(rjPointer* pPointer, rjDoc* pDoc, __int64 defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_uint64(rjPointer* pPointer, rjDoc* pDoc, unsigned __int64 defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_float(rjPointer* pPointer, rjDoc* pDoc, float defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_double(rjPointer* pPointer, rjDoc* pDoc, double defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_string(rjPointer* pPointer, rjDoc* pDoc, const char* defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_get_with_default_bool(rjPointer* pPointer, rjDoc* pDoc, BOOL defaultValue)
{
	return &pPointer->GetWithDefault(*pDoc, (bool)defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set(rjPointer* pPointer, rjDoc* pDoc, rjValue* pValue)
{
	return &pPointer->Set(*pDoc, *pValue);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_int(rjPointer* pPointer, rjDoc* pDoc, int val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_uint(rjPointer* pPointer, rjDoc* pDoc, unsigned val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_int64(rjPointer* pPointer, rjDoc* pDoc, __int64 val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_uint64(rjPointer* pPointer, rjDoc* pDoc, unsigned __int64 val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_float(rjPointer* pPointer, rjDoc* pDoc, float val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_double(rjPointer* pPointer, rjDoc* pDoc, double val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_string(rjPointer* pPointer, rjDoc* pDoc, const char* val)
{
	return &pPointer->Set(*pDoc, val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_set_bool(rjPointer* pPointer, rjDoc* pDoc, BOOL val)
{
	return &pPointer->Set(*pDoc, (bool)val);
}

extern RAPIDJSON_API rjValue* rapidjson_pointer_swap(rjPointer* pPointer, rjDoc* pDoc, rjValue* pValue)
{
	return &pPointer->Swap(*pDoc, *pValue);
}

extern RAPIDJSON_API rjValue* rapidjson_create_value_by_pointer(rjDoc* pDoc, rjPointer* pPointer)
{
	return &rapidjson::CreateValueByPointer(*pDoc, *pPointer);
}

extern RAPIDJSON_API rjValue* rapidjson_create_value_by_pointer_string(rjDoc* pDoc, const char* point)
{
	return &rapidjson::CreateValueByPointer(*pDoc, rjPointer(point));
}

extern RAPIDJSON_API rjValue* rapidjson_get_value_by_pointer(rjValue* root, rjPointer* pPointer, size_t* unresolvedTokenIndex /*= 0*/)
{
	return rapidjson::GetValueByPointer(*root, *pPointer, unresolvedTokenIndex);
}

extern RAPIDJSON_API rjValue* rapidjson_get_value_by_pointer_string(rjValue* root, const char* point, size_t* unresolvedTokenIndex /*= 0*/)
{
	return rapidjson::GetValueByPointer(*root, rjPointer(point), unresolvedTokenIndex);
}

extern RAPIDJSON_API rjValue* rapidjson_get_value_by_pointer_with_default(rjDoc* pDoc, rjPointer* pPointer, rjValue* defaultValue)
{
	return &rapidjson::GetValueByPointerWithDefault(*pDoc, *pPointer, *defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_get_value_by_pointer_with_default_string(rjDoc* pDoc, const char* point, rjValue* defaultValue)
{
	return &rapidjson::GetValueByPointerWithDefault(*pDoc, rjPointer(point), *defaultValue);
}

extern RAPIDJSON_API rjValue* rapidjson_set_value_by_pointer(rjDoc* pDoc, rjPointer* pPointer, rjValue* pValue)
{
	return &rapidjson::SetValueByPointer(*pDoc, *pPointer, *pValue);
}

extern RAPIDJSON_API rjValue* rapidjson_set_value_by_pointer_string(rjDoc* pDoc, const char* point, rjValue* pValue)
{
	return &rapidjson::SetValueByPointer(*pDoc, rjPointer(point), *pValue);
}

extern RAPIDJSON_API rjValue* rapidjson_swap_value_by_pointer(rjDoc* pDoc, rjPointer* pPointer, rjValue* pValue)
{
	return &rapidjson::SwapValueByPointer(*pDoc, *pPointer, *pValue);
}

extern RAPIDJSON_API rjValue* rapidjson_swap_value_by_pointer_string(rjDoc* pDoc, const char* point, rjValue* pValue)
{
	return &rapidjson::SwapValueByPointer(*pDoc, rjPointer(point), *pValue);
}

